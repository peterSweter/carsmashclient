//
// Created by peter on 4/13/18.
//

#include "ServiceProvider.h"
std::shared_ptr<NetworkManager> ServiceProvider::networkManager_;
std::shared_ptr<ScaleManager> ServiceProvider::scaleManager_;
std::string ServiceProvider::nick_ = "";
std::string ServiceProvider::serverIP_ = "ws://127.0.0.1:8080";