//
// Created by peter on 5/29/18.
//

#include <cocos/base/CCDirector.h>
#include "ScaleManager.h"


float const ScaleManager::baseWidth = 1280.0;
float const ScaleManager::baseHeight = 720.0;
float const ScaleManager::serverBaseWidth = 12.8;

float ScaleManager::getScale() {
    return (baseWidth/serverBaseWidth)*(cocos2d::Director::getInstance()->getVisibleSize().width / baseWidth);
}

cocos2d::Vec2 & ScaleManager::scaleVec(cocos2d::Vec2 &vec) {

    vec.x *= getScale();
    vec.y *= getScale();

    return vec;
}

ScaleManager::ScaleManager() {

}
