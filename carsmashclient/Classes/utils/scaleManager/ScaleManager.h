//
// Created by peter on 5/29/18.
//

#ifndef MYGAME_SCALEMANAGER_H
#define MYGAME_SCALEMANAGER_H


class ScaleManager {

private:

    static const float baseWidth;
    static const float baseHeight;
    static const float serverBaseWidth;

public:
    float getScale();
    cocos2d::Vec2 & scaleVec(cocos2d::Vec2 &vec);

    ScaleManager();
};


#endif //MYGAME_SCALEMANAGER_H
