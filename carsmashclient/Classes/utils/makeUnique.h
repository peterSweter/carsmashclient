//
// Created by peter on 4/13/18.
//

#ifndef MYGAME_MAKEUNIQUE_H
#define MYGAME_MAKEUNIQUE_H

#include <utility>
#include <memory>
namespace std {
    template<typename T, typename... Args>
    std::unique_ptr<T> make_unique(Args &&... args) {
        return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }
}

#endif //MYGAME_MAKEUNIQUE_H
