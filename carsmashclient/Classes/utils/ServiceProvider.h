//
// Created by peter on 4/13/18.
//

#ifndef MYGAME_SERVICEPROVIDER_H
#define MYGAME_SERVICEPROVIDER_H


#include <network/NetworkManager.h>
#include <utils/scaleManager/ScaleManager.h>

class ServiceProvider {
public:
   static std::shared_ptr<NetworkManager> networkManager_;
   static std::shared_ptr<ScaleManager> scaleManager_;
   static  std::string nick_;
   static std::string serverIP_;
};


#endif //MYGAME_SERVICEPROVIDER_H
