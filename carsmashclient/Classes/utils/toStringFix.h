//
// Created by peter on 4/11/18.
//

#ifndef MYGAME_TOSTRINGFIX_H
#define MYGAME_TOSTRINGFIX_H
#include <string>
#include <sstream>


namespace std
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }


    template < typename T > int stoi( T const  str  )
    {
        int toRet;
        std::istringstream stm(str) ;
        stm >>toRet;
        return toRet;
    }



}
#endif //MYGAME_TOSTRINGFIX_H
