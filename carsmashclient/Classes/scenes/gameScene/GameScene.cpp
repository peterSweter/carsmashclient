//
// Created by peter on 4/10/18.
//


#include <utils/ServiceProvider.h>
#include "cocos2d.h"
#include "GameScene.h"

using namespace cocos2d;

Scene *GameScene::createScene() {

    return GameScene::create();
}

bool GameScene::init() {

    if(!Scene::init()){
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();



    auto tmpLabel = Label::createWithSystemFont("Game Screen",  "Arial", 36);
    tmpLabel->setAnchorPoint(Vec2(0.5, 0.5));

    tmpLabel->setPosition(visibleSize.width/2  +origin.x, visibleSize.height/2 + origin.y);

    this->addChild(tmpLabel);

    gameController_ = std::make_shared<GameController>( );
    arrows_ = std::make_shared<Arrows>(this, gameController_);

    this->gameRenderer_ = std::make_shared<GameRenderer>(this);
    gameController_->registerRenderer(gameRenderer_.get());
    gameController_->sendGameSessionInitData(ServiceProvider::nick_, "baseModel");





    return true;

}
