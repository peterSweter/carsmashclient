//
// Created by peter on 6/26/18.
//

#include <cocos/base/CCDirector.h>
#include <cocos/ui/UILayout.h>
#include <iostream>
#include "UpgradeSection.h"
#include "UpgradeBtn.h"

using namespace cocos2d;
UpgradeSection::UpgradeSection(cocos2d::Scene *scene) : scene_(scene) {

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    upgradePoints_ = labelPool_.getLabel();


    float offset = 40;
    for(std::string tag : statKeys){

       buttons.insert(std::make_pair(tag,UpgradeBtn(scene, &labelPool_, statID[tag], tag, offset, visibleSize.height - 40)));
    }

    upgradePoints_->setString("upgradePoints : 0 ");
    scene_->addChild(upgradePoints_, 4);
    offset+=50;
    upgradePoints_->setPosition(cocos2d::Vec2(offset, visibleSize.height - 30));
}

void UpgradeSection::receiveMessage(const rapidjson::Value & statJson) {
    for(std::string &  tagID : statKeys){

        int lvl = statJson.FindMember(tagID.c_str())->value.GetInt();


        buttons.find(tagID)->second.setValue(lvl);
    }

    int upgradePoints = statJson.FindMember("upgradePoints")->value.GetInt();
    std::cout <<  " -------------------> upgrade points "  << upgradePoints << std::endl;
    upgradePoints_->setString("upgradePoints: " + std::to_string(upgradePoints));
}
