//
// Created by peter on 6/26/18.
//

#ifndef MYGAME_UPGRADESECTION_H
#define MYGAME_UPGRADESECTION_H


#include <scenes/gameScene/renderer/LabelPool.h>
#include "UpgradeBtn.h"

class UpgradeSection {
public:

    LabelPool  labelPool_;
    cocos2d::DrawNode* drawNode_;
    cocos2d::Scene * scene_;
    cocos2d::Label * upgradePoints_;

    std::map<std::string, int> statID = {{"maxHealth",    0},
                                           {"healthRegen",  1},
                                           {"speed",        2},
                                           {"acceleration", 3}
    };

    UpgradeSection(  cocos2d::Scene * scene);

    std::vector<std::string> statKeys =   {"maxHealth","healthRegen",  "speed","acceleration"};
    std::map<std::string, UpgradeBtn> buttons;

    void receiveMessage(rapidjson::Value const &updateJson);
};


#endif //MYGAME_UPGRADESECTION_H
