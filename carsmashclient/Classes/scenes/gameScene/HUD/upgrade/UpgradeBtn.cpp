//
// Created by peter on 6/26/18.
//

#include <cocos/ui/UILayout.h>
#include <utils/ServiceProvider.h>
#include "UpgradeBtn.h"
#include "utils/toStringFix.h"

UpgradeBtn::UpgradeBtn(cocos2d::Scene *scene, LabelPool *labelPool, int id, std::string text, float &offset, float y) : tag(text), id(id){
    btn =  cocos2d::ui::Button::create("plus.png");
    int myID = id;
    btn->addTouchEventListener([this, myID](cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {

            case cocos2d::ui::Widget::TouchEventType::BEGAN:
                sendUpgradeRequest(myID);
                break;
            case cocos2d::ui::Widget::TouchEventType::ENDED:

                break;
            default:
                break;
        }

    });

    scene->addChild(btn,4);

    btn->setPosition(cocos2d::Vec2(offset, y));
    offset+= btn->getContentSize().width +30;
    label_ =labelPool->getLabel();

    label_->setString(text);

    scene->addChild(label_, 4);

    label_->setPosition(cocos2d::Vec2(offset, y+10));
    offset+=80;





}

void UpgradeBtn::sendUpgradeRequest(int idToSend) {

    auto msg = std::make_shared<rapidjson::Document>();
    msg->SetObject();
    msg->AddMember("t", "u", msg->GetAllocator());
    msg->AddMember("v",idToSend, msg->GetAllocator());
    ServiceProvider::networkManager_->sendMessage(msg);
}

void UpgradeBtn::setValue(int val) {
    std::cout << "setValue " << val << std::endl;
    this->label_->setString(tag+ ": " + std::to_string(val));
}
