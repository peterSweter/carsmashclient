//
// Created by peter on 6/26/18.
//

#ifndef MYGAME_UPGRADEBTN_H
#define MYGAME_UPGRADEBTN_H


#include <cocos/2d/CCScene.h>
#include <scenes/gameScene/renderer/LabelPool.h>
#include <cocos/ui/UIButton.h>
#include <network/EventObserverI.h>

class UpgradeBtn {
public:
    cocos2d::Label * label_;
    cocos2d::ui::Button * btn;
    std::string tag;
    int id;




    UpgradeBtn(cocos2d::Scene *scene, LabelPool *labelPool, int id, std::string text, float &offset, float y);
    void sendUpgradeRequest(int);
    void setValue(int val);
};


#endif //MYGAME_UPGRADEBTN_H
