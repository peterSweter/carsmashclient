//
// Created by peter on 4/11/18.
//

#include <cocos/ui/UILayout.h>
#include <cocos/ui/UIButton.h>
#include "Arrows.h"

using namespace cocos2d;

Arrows::Arrows(cocos2d::Scene *scene, std::shared_ptr<ArrowsObserverI> observerI) : observerI_(observerI)  {

    currentState_ = {0,0};
    float btnScale = 1.5;


    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto leftBox = ui::Layout::create();
    leftBox->setLayoutType(ui::Layout::Type::VERTICAL);
    leftBox->setBackGroundColor(Color3B::GREEN);
    leftBox->setAnchorPoint(Vec2(0.0, 0.0));
    leftBox->setPosition(Vec2(10  *btnScale+ origin.x,170 *btnScale + origin.y));

    auto upBtn = ui::Button::create("arrowUp.png");
    auto downBtn = ui::Button::create("arrowDown.png");

    upBtn->setScale(btnScale);
    downBtn->setScale(btnScale);

    // creating a keyboard event listener
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(Arrows::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(Arrows::onKeyReleased, this);

    scene->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, scene);



    upBtn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                currentState_.second+=1;
                observerI_->update(40, 'd');

                break;
            case ui::Widget::TouchEventType::ENDED:
                currentState_.second-=1;
                observerI_->update(40, 'u');

                break;
            default:
                break;
        }

    });

    downBtn->addTouchEventListener([&, this](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                currentState_.second-=1;
                observerI_->update(38, 'd');
                break;
            case ui::Widget::TouchEventType::ENDED:
                currentState_.second+=1;
                observerI_->update(38, 'u');
                break;
            default:
                break;
        }


    });

    //upBtn->setPosition(Vec2(upBtn->getContentSize().width/2, upBtn->getContentSize().height/2));
    leftBox->addChild(upBtn);
    leftBox->addChild(downBtn);

    auto rightBox = ui::Layout::create();
    rightBox->setLayoutType(ui::Layout::Type::HORIZONTAL);
    rightBox->setBackGroundColor(Color3B::GREEN);
    rightBox->setAnchorPoint(Vec2(0.0, 0.0));
    rightBox->setPosition(Vec2(visibleSize.width - 170 *btnScale + origin.x,90 *btnScale + origin.y));

    auto rightBtn = ui::Button::create("arrowRight.png");
    auto leftBtn = ui::Button::create("arrowLeft.png");

    rightBtn->setScale(btnScale);
    leftBtn->setScale(btnScale);

    rightBtn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                currentState_.first+=1;
                observerI_->update(39, 'd');
                break;
            case ui::Widget::TouchEventType::ENDED:
                currentState_.first-=1;
                observerI_->update(39, 'u');

                break;
            default:
                break;
        }

    });

    leftBtn->addTouchEventListener([&, this](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                currentState_.first-=1;
                observerI_->update(37,'d');
                break;
            case ui::Widget::TouchEventType::ENDED:
                currentState_.first+=1;
                observerI_->update(37, 'u');


                break;
            default:
                break;
        }

    });

    //upBtn->setPosition(Vec2(upBtn->getContentSize().width/2, upBtn->getContentSize().height/2));
    rightBox->addChild(leftBtn);
    rightBox->addChild(rightBtn);

    scene->addChild(leftBox);
    scene->addChild(rightBox);

    scene->reorderChild(leftBox, 4);
    scene->reorderChild(rightBox, 4);

}




// Implementation of the keyboard event callback function prototype
void Arrows::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{

    switch(keyCode){
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
        case EventKeyboard::KeyCode::KEY_A:
            observerI_->update(37, 'd');
            break;
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
        case EventKeyboard::KeyCode::KEY_D:
            observerI_->update(39, 'd');
            break;
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
        case EventKeyboard::KeyCode::KEY_W:
            observerI_->update(40, 'd');
            break;
        case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
        case EventKeyboard::KeyCode::KEY_S:
            observerI_->update(38, 'd');
            break;
    }
}

void Arrows::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{


    switch(keyCode){
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
        case EventKeyboard::KeyCode::KEY_A:
            observerI_->update(37, 'u');
            break;
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
        case EventKeyboard::KeyCode::KEY_D:
            observerI_->update(39, 'u');
            break;
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
        case EventKeyboard::KeyCode::KEY_W:
            observerI_->update(40, 'u');
            break;
        case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
        case EventKeyboard::KeyCode::KEY_S:
            observerI_->update(38, 'u');
            break;
    }
}