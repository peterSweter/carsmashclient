//
// Created by peter on 4/11/18.
//

#ifndef MYGAME_ARROWOBSERVERI_H
#define MYGAME_ARROWOBSERVERI_H


#include <utility>

class ArrowsObserverI {
public:
    virtual void update(int keyCode, char state) = 0;

};


#endif //MYGAME_ARROWOBSERVERI_H
