//
// Created by peter on 4/11/18.
//

#ifndef MYGAME_ARROWS_H
#define MYGAME_ARROWS_H

#include "cocos2d.h"
#include "ArrowsObserverI.h"


class Arrows  : public std::enable_shared_from_this<Arrows> {
public:
    Arrows(cocos2d::Scene * scene, std::shared_ptr<ArrowsObserverI>);
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
private:
    std::pair<int, int> currentState_;
    std::shared_ptr<ArrowsObserverI> observerI_;
};


#endif //MYGAME_ARROWS_H
