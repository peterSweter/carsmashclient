//
// Created by peter on 6/17/18.
//

#ifndef MYGAME_TOPTABLERENDERER_H
#define MYGAME_TOPTABLERENDERER_H


#include <cocos/2d/CCScene.h>
#include <utils/rapidjson/document.h>
#include <cocos/ui/UILayout.h>
#include <cocos/2d/CCLabel.h>

class TopTableRenderer {
private:

    cocos2d::Scene * scene_;
    cocos2d::ui::Layout * layout_;
    std::vector<cocos2d::Label *> labels_;

public:
    TopTableRenderer(cocos2d::Scene *scene_);
    void update(std::shared_ptr<rapidjson::Document> jsonMessage);

public:

};


#endif //MYGAME_TOPTABLERENDERER_H
