//
// Created by peter on 5/28/18.
//

#ifndef MYGAME_COLORSPICKER_H
#define MYGAME_COLORSPICKER_H


#include <cocos/base/ccTypes.h>
#include <string>
#include <set>

class ColorsPicker {
    public:
     std::map<std::string, cocos2d::Color4F> colors_;
     ColorsPicker();
};


#endif //MYGAME_COLORSPICKER_H
