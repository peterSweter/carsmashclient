//
// Created by peter on 4/13/18.
//

#ifndef MYGAME_GAMERENDERER_H
#define MYGAME_GAMERENDERER_H


#include <network/EventObserverI.h>
#include <cocos/2d/CCSprite.h>

#include "cocos2d.h"
#include "utils/rapidjson/document.h"
#include "utils/toStringFix.h"
#include "EntityRenderer.h"
#include "TopTableRenderer.h"
#include "MiniMapRenderer.h"
#include "LabelPool.h"

#define ROUNDF(f, c) (((float)((int)((f) * (c))) / (c)))

class GameRenderer : public EventObserverI {
public:

    GameRenderer(cocos2d::Scene *);

    void receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) override;

    cocos2d::DrawNode* drawNode_;
    cocos2d::DrawNode* drawNodeGround_;
    cocos2d::DrawNode* uiDrawNode_;

    std::shared_ptr<UpgradeSection> upgradeSection_;
    EntityRenderer entityRenderer_;
    TopTableRenderer topTableRenderer_;
    MiniMapRenderer miniMapRenderer_;
   static cocos2d::Vec2 getCarPosition(rapidjson::Value const & carJson);

private:
    cocos2d::Sprite * car_;
    cocos2d::Scene * scene_;
    const float gridSize = 0.5;

    void drawGround(cocos2d::Vec2 & playerPosition);

};


#endif //MYGAME_GAMERENDERER_H
