//
// Created by peter on 6/18/18.
//

#ifndef MYGAME_LABELPOOL_H
#define MYGAME_LABELPOOL_H


#include <cocos/2d/CCLabel.h>

class LabelPool {
    int currentIndex=0;
    std::vector<cocos2d::Label*> labels_;
public:
    void reset();
    LabelPool();
    cocos2d::Label * getLabel();
};


#endif //MYGAME_LABELPOOL_H
