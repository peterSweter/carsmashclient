//
// Created by peter on 5/28/18.
//

#include <iostream>
#include <utils/rapidjson/stringbuffer.h>
#include <utils/rapidjson/writer.h>
#include <cocos/base/CCDirector.h>
#include <utils/ServiceProvider.h>
#include "utils/scaleManager/ScaleManager.h"
#include "EntityRenderer.h"
#include "ColorsPicker.h"
#include "GameRenderer.h"
#include "utils/toStringFix.h"


EntityRenderer::EntityRenderer(cocos2d::Scene *scene_,   cocos2d::DrawNode* drawNode) : scene_(scene_),
                                                                                      drawNode_(drawNode){



}

void EntityRenderer::drawEntity(cocos2d::Vec2 &playerPos, rapidjson::Value const &entityJson) {


    bool isCar = entityJson.HasMember("hp");

    for (auto const &  bodyJson : entityJson["bodies"].GetArray()) {
        drawBody(playerPos, bodyJson);
    }

    if(isCar){

        auto carPos = GameRenderer::getCarPosition(entityJson);
        carPos -= playerPos;

        ServiceProvider::scaleManager_->scaleVec(carPos);

        carPos.x += cocos2d::Director::getInstance()->getVisibleSize().width/2;
        carPos.y += cocos2d::Director::getInstance()->getVisibleSize().height/2;

        drawHp(carPos, entityJson["hp"].GetFloat());
        drawExp(carPos, entityJson["exp"].GetFloat());

        carPos.y+=20;

        cocos2d::Label *  label = labelPool_.getLabel();
        label->setVisible(true);
        label->setColor(cocos2d::Color3B::BLACK);
        label->setString(std::to_string(entityJson["nick"].GetString()) + " " + std::to_string(entityJson["points"].GetFloat()));
        label->setSystemFontSize(14.0);
        label->setPosition(carPos.x, carPos.y);

        if(label->getParent() == nullptr){
            scene_->addChild(label, 3);
        }


    }

}

void EntityRenderer::drawBody(cocos2d::Vec2 &playerPos, rapidjson::Value const &bodyJson) {


    cocos2d:: Vec2 bodyPos(bodyJson["x"].GetFloat(), bodyJson["y"].GetFloat());
    bodyPos -= playerPos;

    ServiceProvider::scaleManager_->scaleVec(bodyPos);

    bodyPos.x += cocos2d::Director::getInstance()->getVisibleSize().width/2;
    bodyPos.y += cocos2d::Director::getInstance()->getVisibleSize().height/2;

    float bodyAngle =  bodyJson["angle"].GetFloat();

    for (auto const &  fixtureJson : bodyJson["fixtures"].GetArray()) {
        drawFixture(bodyPos, bodyAngle, fixtureJson);
    }


}

void EntityRenderer::drawFixture(cocos2d::Vec2 &bodyPos, float angle,  rapidjson::Value const &fixtureJson) {
/*
    rapidjson::StringBuffer buffer;

    buffer.Clear();

    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    fixtureJson.Accept(writer);

    std::cout << "Draw fixture test " << angle << " " << strdup( buffer.GetString() ) << std::endl;
*/

    std::string shapeType = fixtureJson["shape"].GetString();
    cocos2d::Color4F color = colorsPicker_.colors_[fixtureJson["color"].GetString()];





    if(shapeType == "polygon"){

        auto vertices = jsonPlygonToArray(fixtureJson["vertices"], bodyPos, angle);




        drawNode_->drawPolygon(vertices.data(), vertices.size(), color, 1, cocos2d::Color4F::WHITE);

    }else if(shapeType == "box"){



        auto vertices = jsonBoxToArray(fixtureJson["width"].GetFloat(), fixtureJson["height"].GetFloat(), bodyPos, angle);


             drawNode_->drawPolygon(vertices.data(), vertices.size(), color , 1, cocos2d::Color4F::WHITE);

    }else if(shapeType == "circle"){

        //TODO create this for purpose of smog and barrel shape

    }
}

std::vector<cocos2d::Vec2>
EntityRenderer::jsonPlygonToArray(rapidjson::Value const &verticesJson, cocos2d::Vec2 &bodyPos, float angle) {

    std::vector<cocos2d::Vec2> vertices;

    for (auto const &  vertex : verticesJson.GetArray()) {
        vertices.emplace_back(vertex["x"].GetFloat(), vertex["y"].GetFloat());
        ServiceProvider::scaleManager_->scaleVec(vertices.back());
        vertices.back() += bodyPos;
        vertices[vertices.size()-1] = vertices.back().rotateByAngle(bodyPos,angle);

    }

    return vertices;
}

std::vector<cocos2d::Vec2>
EntityRenderer::jsonBoxToArray(float width, float height, cocos2d::Vec2 &bodyPos, float angle) {

    width *= ServiceProvider::scaleManager_->getScale();
    height *= ServiceProvider::scaleManager_->getScale();

    std::vector<cocos2d::Vec2> vertices  = {
        {bodyPos.x - width, bodyPos.y - height },
            {bodyPos.x + width, bodyPos.y - height },
            {bodyPos.x + width, bodyPos.y + height },
            {bodyPos.x - width, bodyPos.y + height }

    };




    for(auto & v : vertices){
        v = v.rotateByAngle(bodyPos, angle);
    }


    return vertices;

}

void EntityRenderer::printVec2Vector(std::vector<cocos2d::Vec2> const &vec) {
    std::cout << "Debug vector print  " << std::endl;

    for(auto & v : vec){
        std::cout << v.x << " " << v.y << std::endl;
    }

}

void EntityRenderer::drawHp(cocos2d::Vec2 &playerPos, double hpPercent) {

    std::cout << hpPercent << std::endl;

    float width = this->hpWidth * hpPercent * ServiceProvider::scaleManager_->getScale()/2 ;
    float height = this->hpHeight * ServiceProvider::scaleManager_->getScale()/2;
    float radius = 1.4 *  ServiceProvider::scaleManager_->getScale();

    playerPos.y +=radius;

    std::vector<cocos2d::Vec2> vertices  = {
            {playerPos.x - width, playerPos.y - height },
            {playerPos.x + width, playerPos.y - height },
            {playerPos.x + width, playerPos.y + height },
            {playerPos.x - width, playerPos.y + height }

    };

    //TODO move this to ui node
    drawNode_->drawPolygon(vertices.data(), vertices.size(), cocos2d::Color4F::RED, 1, cocos2d::Color4F::WHITE);
}

void EntityRenderer::drawExp(cocos2d::Vec2 &playerPos, double expPercent) {

    std::cout << expPercent << std::endl;

    float width = this->hpWidth * expPercent * ServiceProvider::scaleManager_->getScale()/2 ;
    float height = this->expHeight * ServiceProvider::scaleManager_->getScale()/2;
    float radius = this->hpHeight * ServiceProvider::scaleManager_->getScale()*0.75 ;

    playerPos.y +=radius;

    std::vector<cocos2d::Vec2> vertices  = {
            {playerPos.x - width, playerPos.y - height },
            {playerPos.x + width, playerPos.y - height },
            {playerPos.x + width, playerPos.y + height },
            {playerPos.x - width, playerPos.y + height }

    };

    //TODO move this to ui node
    drawNode_->drawPolygon(vertices.data(), vertices.size(), cocos2d::Color4F::YELLOW, 1, cocos2d::Color4F::WHITE);
}


