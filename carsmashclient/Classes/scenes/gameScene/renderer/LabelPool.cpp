//
// Created by peter on 6/18/18.
//

#include <iostream>
#include "LabelPool.h"

void LabelPool::reset() {
    currentIndex=0;
    for(auto lbl : labels_){
        if(lbl->getParent() != nullptr){
            lbl->setVisible(false);
        }
    }
}


cocos2d::Label *LabelPool::getLabel() {
    std::cout << "current index " << currentIndex << std::endl;


    if(currentIndex >= labels_.size()){
        labels_.push_back(cocos2d::Label::createWithSystemFont("", "Arial" , 15));
    }

    return labels_[currentIndex++];


}

LabelPool::LabelPool() :labels_() {

}
