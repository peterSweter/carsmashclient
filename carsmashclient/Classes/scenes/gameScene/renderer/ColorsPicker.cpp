//
// Created by peter on 5/28/18.
//

#include "ColorsPicker.h"


ColorsPicker::ColorsPicker() {
    colors_= {
            {"red", cocos2d::Color4F::RED},
            {"black", cocos2d::Color4F::BLACK},
            {"white", cocos2d::Color4F::WHITE},
            {"yellow", cocos2d::Color4F::YELLOW},
            {"green", cocos2d::Color4F::GREEN},
            {"brown", cocos2d::Color4F::ORANGE},
            {"purple", cocos2d::Color4F(0.3, 0.4, 0.5, 1.0)}

    };
}
