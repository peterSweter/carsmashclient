//
// Created by peter on 5/30/18.
//

#ifndef MYGAME_DRAWNODEPOOL_H
#define MYGAME_DRAWNODEPOOL_H


#include <cocos/2d/CCDrawNode.h>

class DrawNodePool {
private:
    std::vector<cocos2d::DrawNode*> drawNodes_;
    cocos2d::Scene *  scene_;
public:

    DrawNodePool(cocos2d::Scene *  scene);
    cocos2d::DrawNode * getDrawNode();
    void beginDrawing();
    void finishDawing();

};


#endif //MYGAME_DRAWNODEPOOL_H
