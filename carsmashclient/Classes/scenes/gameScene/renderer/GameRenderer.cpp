//
// Created by peter on 4/13/18.
//

#include <iostream>
#include <utils/rapidjson/stringbuffer.h>
#include <utils/rapidjson/writer.h>
#include <utils/ServiceProvider.h>
#include <scenes/MainMenuScene.h>
#include "GameRenderer.h"

//TODO create structure and hierarchy to redner custom objects, every object will be json object representing box2d sweter standard.
GameRenderer::GameRenderer(cocos2d::Scene *scene) : scene_(scene), drawNode_(cocos2d::DrawNode::create()),
                                                    entityRenderer_(scene, drawNode_), topTableRenderer_(scene),
                                                    miniMapRenderer_(scene) {
    /* ---  // test sprite
    car_ = cocos2d::Sprite::create("car.png");
    car_->setPosition(100, 100);
    scene_->addChild(car_);
    */

    drawNodeGround_ = cocos2d::DrawNode::create();
    uiDrawNode_ = cocos2d::DrawNode::create();

    scene_->addChild(drawNode_);
    scene->addChild(drawNodeGround_);
    scene_->reorderChild(drawNode_, 2);
    scene_->reorderChild(uiDrawNode_, 3);
    scene_->reorderChild(drawNodeGround_, 1);

    this->upgradeSection_ = std::make_shared<UpgradeSection>(scene_);


    // auto myLabel = cocos2d::Label::createWithSystemFont("My Label Text", "Arial", 16);
    // myLabel->setPosition(cocos2d::Director::getInstance()->getVisibleSize().width - 100, cocos2d::Director::getInstance()->getVisibleSize().height -100);


}

void GameRenderer::receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) {


    rapidjson::StringBuffer buffer;

    buffer.Clear();

    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    jsonMessage->Accept(writer);

    // std::cout << "received message " << strdup( buffer.GetString() ) << std::endl;

    if ((*jsonMessage)["t"] == "u") {

        rapidjson::Value const &gameObjects = jsonMessage->FindMember("gameObjects")->value;

        cocos2d::Vec2 playerPosition = this->getCarPosition(gameObjects[0]);

        //std::cout << "player position " << playerPosition.x << " " << playerPosition.y << std::endl;
        drawNode_->clear();
        drawGround(playerPosition);

        entityRenderer_.labelPool_.reset();
        upgradeSection_->receiveMessage(gameObjects[0]["stats"]);

        for (auto const &entityJson : gameObjects.GetArray()) {

            entityRenderer_.drawEntity(playerPosition, entityJson);
        }


    }

    if ((*jsonMessage)["t"] == "tps") {
        topTableRenderer_.update(jsonMessage);
    }

    if ((*jsonMessage)["t"] == "mp") {

        miniMapRenderer_.update(jsonMessage);
    }

    if ((*jsonMessage)["t"] == "over") {

        cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1,MainMenuScene::createScene()));
    }

    return;

    std::cout << "v: " << jsonMessage->FindMember("t")->value.GetString() << " x:  "
              << jsonMessage->FindMember("x")->value.GetInt() << " y: " << jsonMessage->FindMember("y")->value.GetInt()
              << std::endl;
    jsonMessage->FindMember("t");
    if ((*jsonMessage)["t"] == "u") {

        int x = std::stoi(jsonMessage->FindMember("x")->value.GetString());
        int y = std::stoi(jsonMessage->FindMember("y")->value.GetString());

        auto speed = 10;
        car_->setPosition(cocos2d::Vec2(x, y));

    }

}

cocos2d::Vec2 GameRenderer::getCarPosition(rapidjson::Value const &carJson) {

    return cocos2d::Vec2(carJson.FindMember("x")->value.GetFloat(), carJson.FindMember("y")->value.GetFloat());
}




void GameRenderer::drawGround(cocos2d::Vec2 &playerPosition) {

    //offset
    float scale = ServiceProvider::scaleManager_->getScale();
    float smothOfset = 2.0;

    //playerPosition.x = ROUNDF(playerPosition.x,6);
    //playerPosition.y = ROUNDF(playerPosition.y,6);


    float offsetX = (int) (playerPosition.x * scale - (int) (playerPosition.x * scale)) -
                    ((int) (playerPosition.x * scale) % ((int) (gridSize * scale)));
    float offsetY = (int) (playerPosition.y * scale - (int) (playerPosition.y * scale)) -
                    ((int) (playerPosition.y * scale) % ((int) (gridSize * scale)));

    std::cout << std::endl << offsetX << " " << offsetY << std::endl << std::endl;

    drawNodeGround_->clear();

    std::vector<cocos2d::Vec2> vertices = {
            {0.0,                                                      0.0},
            {cocos2d::Director::getInstance()->getVisibleSize().width, 0},
            {cocos2d::Director::getInstance()->getVisibleSize().width, cocos2d::Director::getInstance()->getVisibleSize().height},
            {0.0,                                                      cocos2d::Director::getInstance()->getVisibleSize().height}

    };

    drawNodeGround_->drawPolygon(vertices.data(), vertices.size(), cocos2d::Color4F::GRAY, 0, cocos2d::Color4F::WHITE);


    for (float y = 0; y < cocos2d::Director::getInstance()->getVisibleSize().height; y += this->gridSize *
                                                                                          ServiceProvider::scaleManager_->getScale()) {
        cocos2d::Vec2 aPoint = {0.0, y + offsetY};
        cocos2d::Vec2 bPoint = {cocos2d::Director::getInstance()->getVisibleSize().width, y + offsetY};

        drawNodeGround_->drawLine(aPoint, bPoint, cocos2d::Color4F::BLACK);

    }

    for (float x = 0; x < cocos2d::Director::getInstance()->getVisibleSize().width; x += this->gridSize *
                                                                                         ServiceProvider::scaleManager_->getScale()) {
        cocos2d::Vec2 aPoint = {x + offsetX, 0.0};
        cocos2d::Vec2 bPoint = {x + offsetX, cocos2d::Director::getInstance()->getVisibleSize().height};

        drawNodeGround_->drawLine(aPoint, bPoint, cocos2d::Color4F::BLACK);

    }

}
