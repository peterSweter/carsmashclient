//
// Created by peter on 5/28/18.
//

#ifndef MYGAME_ENTITYRENDERER_H
#define MYGAME_ENTITYRENDERER_H


#include <cocos/2d/CCScene.h>
#include <external/json/document.h>
#include <cocos/2d/CCDrawNode.h>

#include "ColorsPicker.h"
#include "LabelPool.h"
#include <scenes/gameScene/HUD/upgrade/UpgradeSection.h>

class EntityRenderer {
public:

    cocos2d::Scene * scene_;
    cocos2d::DrawNode* drawNode_;
    cocos2d::DrawNode* uiDrawNode_;

    ColorsPicker colorsPicker_;
    float hpWidth = 1.8;
    float hpHeight = 0.15;
    float expHeight = 0.035;

    LabelPool labelPool_;



    EntityRenderer(cocos2d::Scene *scene_,  cocos2d::DrawNode* drawNode);

    void drawEntity(cocos2d::Vec2 &playerPos, rapidjson::Value const &entityJson);
    void drawBody(cocos2d::Vec2 &playerPos, rapidjson::Value const &bodyJson);
    void drawFixture(cocos2d::Vec2 &bodyPos, float angle,  rapidjson::Value const &fixtureJson);

    std::vector<cocos2d::Vec2>
    jsonPlygonToArray(rapidjson::Value const &verticesJson, cocos2d::Vec2 &bodyPos, float angle);
    std::vector<cocos2d::Vec2> jsonBoxToArray(float width, float height, cocos2d::Vec2 &bodyPos, float angle);

    void drawPolygon();
    void drawCircle();
    void drawBox();

    void drawHp(cocos2d::Vec2 &playerPos, double hpPercent);

    void printVec2Vector(std::vector<cocos2d::Vec2> const & vec);
    void drawExp(cocos2d::Vec2 &playerPos, double expPercent);


};


#endif //MYGAME_ENTITYRENDERER_H
