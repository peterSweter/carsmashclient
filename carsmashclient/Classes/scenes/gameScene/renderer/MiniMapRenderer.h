//
// Created by peter on 6/17/18.
//

#ifndef MYGAME_MINIMAPRENDERER_H
#define MYGAME_MINIMAPRENDERER_H


#include <cocos/2d/CCDrawNode.h>
#include <utils/rapidjson/document.h>

class MiniMapRenderer {

private:

    float realWidth_ = 20.0;
    float realHeight_ = 20.0;
    cocos2d::DrawNode* uiDrawNode_;
    cocos2d::Scene * scene_;

public:

    MiniMapRenderer(cocos2d::Scene *);
    void update(std::shared_ptr<rapidjson::Document> jsonMessage);
};


#endif //MYGAME_MINIMAPRENDERER_H
