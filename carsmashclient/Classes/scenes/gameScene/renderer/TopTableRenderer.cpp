//
// Created by peter on 6/17/18.
//

#include <cocos/base/CCDirector.h>
#include "TopTableRenderer.h"
#include "utils/toStringFix.h"

TopTableRenderer::TopTableRenderer(cocos2d::Scene *scene) : scene_(scene) {

    auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
    cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();

    float topOffset = visibleSize.height;

    layout_ = cocos2d::ui::Layout::create();
    layout_->setLayoutType(cocos2d::ui::Layout::Type::VERTICAL);
    layout_->setBackGroundColor(cocos2d::Color3B::GREEN);
    layout_->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));

    float offsetTop = 10.0;

    for(int i=0; i <10; i++){
        labels_.push_back(cocos2d::Label::createWithSystemFont("1. test label", "Arial" , 24));
        layout_->addChild(labels_.back());

        labels_.back()->setPosition(10, offsetTop-=24);
        labels_.back()->setColor(cocos2d::Color3B::BLACK);
    }

    layout_->setPosition(cocos2d::Vec2(visibleSize.width - 150 , visibleSize.height  -25));
    scene_->addChild(layout_,4);

}

void TopTableRenderer::update(std::shared_ptr<rapidjson::Document> jsonMessage) {
    rapidjson::Value const &scoreArray = jsonMessage->FindMember("score_table")->value;


    int index=1;

    for (auto const &entityRecord:scoreArray.GetArray()) {
        labels_[index-1]->setVisible(true);
        labels_[index-1]->setString(std::to_string(index) + ". " + std::string(entityRecord.FindMember("nick")->value.GetString()) + " " + std::string(entityRecord.FindMember("score")->value.GetString()));
        index++;
    }

    for(index--;index < 10; index++){
        labels_[index]->setVisible(false);
    }
}
