//
// Created by peter on 6/17/18.
//

#include <utils/ServiceProvider.h>
#include "MiniMapRenderer.h"

MiniMapRenderer::MiniMapRenderer(cocos2d::Scene * scene) : scene_(scene) {
    uiDrawNode_ = cocos2d::DrawNode::create();
    scene_->addChild(uiDrawNode_, 4);
}

void MiniMapRenderer::update(std::shared_ptr<rapidjson::Document> jsonMessage) {

    float scale =  ServiceProvider::scaleManager_->getScale();
    scale/=12;

    uiDrawNode_->clear();
    auto screenDim = cocos2d::Director::getInstance()->getVisibleSize();
    float offsetBottom = 200;

    std::vector<cocos2d::Vec2> vertices  = {
            {screenDim.width -realWidth_ * scale,  offsetBottom + realHeight_ * scale  },
            {screenDim.width ,  offsetBottom + realHeight_ * scale  },
            {screenDim.width ,  offsetBottom  },
            {screenDim.width -realWidth_ * scale,  offsetBottom   }



    };

    auto alphaGrey = cocos2d::Color4F(128.0, 128.0, 128.0, 64.0);
    uiDrawNode_->drawPolygon(vertices.data(), vertices.size(),  alphaGrey, 0, cocos2d::Color4F::WHITE);

    cocos2d::Vec2 mapOrigin(screenDim.width -realWidth_ * scale * 0.5 ,  offsetBottom + realHeight_ * scale * 0.5 );


    rapidjson::Value const &positionsArray = jsonMessage->FindMember("positions_table")->value;


    float dotSize = 5;


    for (auto const &entityRecord : positionsArray.GetArray()) {

        float posX = entityRecord.FindMember("x")->value.GetFloat() * scale + mapOrigin.x;
        float posY = entityRecord.FindMember("y")->value.GetFloat() * scale + mapOrigin.y;



        std::vector<cocos2d::Vec2> vertices  = {
                {posX, posY},
                {posX + dotSize, posY},
                {posX + dotSize, posY + dotSize},
                {posX, posY  + dotSize}



        };

        uiDrawNode_->drawPolygon(vertices.data(), vertices.size(), cocos2d::Color4F::RED, 0, cocos2d::Color4F::WHITE);

    }


}
