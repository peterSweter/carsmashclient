//
// Created by peter on 4/11/18.
//

#ifndef MYGAME_CONTROLLER_H
#define MYGAME_CONTROLLER_H


#include <scenes/gameScene/HUD/ArrowsObserverI.h>
#include <cocos/2d/CCSprite.h>
#include <network/EventObserverI.h>
#include <scenes/gameScene/renderer/GameRenderer.h>
#include "cocos2d.h"

class GameController : public ArrowsObserverI, public EventObserverI{
public:
    void update(int keyCode, char state) override;

    explicit GameController();

    void receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) override;
    void registerRenderer(GameRenderer * gameRenderer);
    void sendGameSessionInitData(std::string nickname, std::string carModelID);

private:
    GameRenderer * gameRenderer_;




};


#endif //MYGAME_CONTROLLER_H
