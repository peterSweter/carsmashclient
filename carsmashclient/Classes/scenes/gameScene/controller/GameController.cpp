//
// Created by peter on 4/11/18.
//

#include <utils/ServiceProvider.h>
#include <iostream>
#include "GameController.h"
#include "utils/toStringFix.h"

using namespace cocos2d;

void GameController::update(int keyCode, char state) {
   //TODO send data to network


    auto msg = std::make_shared<rapidjson::Document>();
    msg->SetObject();


    msg->AddMember("t", "k", msg->GetAllocator());
    msg->AddMember("v", keyCode, msg->GetAllocator());
    msg->AddMember("s", state, msg->GetAllocator());





    ServiceProvider::networkManager_->sendMessage(msg);
}




GameController::GameController() {

    ServiceProvider::networkManager_->registerObserver(this);




}

void GameController::receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) {
    gameRenderer_->receiveMessage(jsonMessage);
}

void GameController::registerRenderer(GameRenderer *gameRenderer) {
    this->gameRenderer_ = gameRenderer;
}

void GameController::sendGameSessionInitData(std::string nickname, std::string carModelID) {
    auto msg = std::make_shared<rapidjson::Document>();
    msg->SetObject();


    msg->AddMember("t", "n", msg->GetAllocator());
    msg->AddMember("nickname", rapidjson::StringRef(nickname.c_str()), msg->GetAllocator());
    msg->AddMember("carModelID", rapidjson::StringRef(carModelID.c_str()), msg->GetAllocator());


    std::cout << "sending pressed keycode to server [1]" <<std::endl;


    ServiceProvider::networkManager_->sendMessage(msg);
}
