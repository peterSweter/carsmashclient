//
// Created by peter on 4/10/18.
//

#ifndef MYGAME_GAMESCENE_H
#define MYGAME_GAMESCENE_H


#include <cocos/2d/CCScene.h>
#include <scenes/gameScene/HUD/Arrows.h>
#include <scenes/gameScene/controller/GameController.h>
#include <scenes/gameScene/HUD/upgrade/UpgradeSection.h>

class GameScene : public cocos2d::Scene{
public:
    static Scene *createScene();
    virtual bool init();

    CREATE_FUNC(GameScene);

private:
    std::string nick_;
    std::shared_ptr<GameController> gameController_;
    std::shared_ptr<GameRenderer> gameRenderer_;
    std::shared_ptr<Arrows> arrows_;
};


#endif //MYGAME_GAMESCENE_H
