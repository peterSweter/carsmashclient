//
// Created by peter on 4/10/18.
//

#ifndef MYGAME_MAINMENUSCENE_H
#define MYGAME_MAINMENUSCENE_H


#include <cocos/ui/UITextField.h>
#include "cocos/cocos2d.h"

class MainMenuScene : public cocos2d::Scene {

public:

    cocos2d::ui::TextField * textField_;
    static cocos2d::Scene * createScene();
    virtual bool init();


    CREATE_FUNC(MainMenuScene);


};


#endif //MYGAME_MAINMENUSCENE_H
