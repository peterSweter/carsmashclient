//
// Created by peter on 4/10/18.
//

#include <iostream>
#include <utils/ServiceProvider.h>
#include "MainMenuScene.h"
#include "ui/CocosGUI.h"
#include "scenes/gameScene/GameScene.h"
#include "utils/rapidjson/document.h"
#include "utils/rapidjson/writer.h"
#include "utils/rapidjson/stringbuffer.h"
#include "utils/toStringFix.h"

using namespace cocos2d;


cocos2d::Scene * MainMenuScene::createScene() {
    return MainMenuScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}


bool MainMenuScene::init() {

    std::string testOwy;


    const char* json = "{\"project\":\"rapidjson\",\"stars\":10}";
    rapidjson::Document d;
    d.Parse(json);

    if(!Scene::init()){
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    float topOffset = visibleSize.height;

    auto label = Label::createWithSystemFont("Car Smash 0.0", "Arial" , 36);
    label->enableOutline(Color4B::GREEN, 1);

    label->setAnchorPoint(Vec2(0.5, 0.5));

    topOffset-= label->getContentSize().height + 30;

    label->setPosition(visibleSize.width/2 + origin.x, topOffset + origin.y);

    auto startGameButton = ui::Button::create("playButton.png", "playButtonClicked.png");

    topOffset-=startGameButton->getContentSize().height + 50;
    startGameButton->setPosition(Vec2(visibleSize.width/2 + origin.x, topOffset + origin.y));
    startGameButton->setScale(2.0);

    textField_ = ui::TextField::create("YourNick","Arial",48);


    startGameButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {

            case ui::Widget::TouchEventType::BEGAN:
            ServiceProvider::nick_ = textField_->getString();
            if(ServiceProvider::nick_ == ""){
                ServiceProvider::nick_="guest";
            }

            Director::getInstance()->replaceScene(TransitionFade::create(1, GameScene::createScene()));
                break;
            case ui::Widget::TouchEventType::ENDED:

                break;
            default:
                break;
        }
    });




    topOffset -= textField_->getContentSize().height +120;
    textField_->setPosition(Vec2(visibleSize.width/2 + origin.x, topOffset + origin.y));

    this->addChild(label);
    this->addChild(startGameButton);
    this->addChild(textField_);

    return true;
}




