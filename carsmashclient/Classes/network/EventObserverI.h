//
// Created by peter on 4/13/18.
//

#ifndef MYGAME_EVENTOBSERVERI_H
#define MYGAME_EVENTOBSERVERI_H

#include <memory>
#include "utils/rapidjson/document.h"

class EventObserverI {
public:
    virtual void receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) = 0;

};


#endif //MYGAME_EVENTOBSERVERI_H
