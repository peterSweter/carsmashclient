//
// Created by peter on 4/6/18.
//

#ifndef MYGAME_SOCKETDELEGATE_H
#define MYGAME_SOCKETDELEGATE_H

#include "network/WebSocket.h"
#include "cocos2d.h"
#include "EventObserverI.h"
#include <iostream>

class SocketDelegate : public cocos2d::network::WebSocket::Delegate {

private:
    EventObserverI *eventObserverI_;

public:


    void registerObserver(EventObserverI * evetnObserver);

    void onOpen(cocos2d::network::WebSocket *ws) override;

    void onMessage(cocos2d::network::WebSocket *ws, const cocos2d::network::WebSocket::Data &data) override;

    void onClose(cocos2d::network::WebSocket *ws) override;

    void onError(cocos2d::network::WebSocket *ws, const cocos2d::network::WebSocket::ErrorCode &error) override;

};


#endif //MYGAME_SOCKETDELEGATE_H
