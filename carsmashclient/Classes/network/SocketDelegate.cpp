//
// Created by peter on 4/6/18.
//


#include "SocketDelegate.h"



void SocketDelegate::onOpen(cocos2d::network::WebSocket *ws) {

    std::cout << "Connection Established" << std::endl;
   //ws->send("{ \"t\":\"n\", \"v\":\"guest\" }");


}

void SocketDelegate::onMessage(cocos2d::network::WebSocket *ws, const cocos2d::network::WebSocket::Data &data) {
    auto jsonMsg = std::make_shared<rapidjson::Document>();

    jsonMsg->Parse(data.bytes);
    eventObserverI_->receiveMessage(jsonMsg);
}

void SocketDelegate::onClose(cocos2d::network::WebSocket *ws) {

}

void SocketDelegate::onError(cocos2d::network::WebSocket *ws, const cocos2d::network::WebSocket::ErrorCode &error) {

}

void SocketDelegate::registerObserver(EventObserverI *evetnObserver) {
    this->eventObserverI_ = evetnObserver;
}
