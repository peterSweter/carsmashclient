//
// Created by peter on 4/13/18.
//

#ifndef MYGAME_NETWORKMANAGER_H
#define MYGAME_NETWORKMANAGER_H


#include "EventObserverI.h"
#include "SocketDelegate.h"

class NetworkManager : public EventObserverI{
private:

    EventObserverI * eventObserverI_;
    std::unique_ptr<SocketDelegate> socketDelegate_;
    std::unique_ptr<cocos2d::network::WebSocket> webSocket_;
    std::vector<std::string> protocols_;
    std::string url_;

public:

    NetworkManager(std::string url);
    void registerObserver(EventObserverI * eventObserverI);
    void unregisterObserver();
    void sendMessage(std::shared_ptr<rapidjson::Document> jsonMessage);
    void sendMessage(std::shared_ptr<std::string> strMessage);
    void receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) override;
    void initConnection();
};


#endif //MYGAME_NETWORKMANAGER_H
