//
// Created by peter on 4/13/18.
//

#include "NetworkManager.h"
#include <utility>
#include <utils/rapidjson/stringbuffer.h>
#include <utils/rapidjson/writer.h>
#include "utils/makeUnique.h"
#include "cocos2d.h"

NetworkManager::NetworkManager(std::string url="ws://localhost:8080") : url_(std::move(url)){

    protocols_= {"rfc6455"};
    webSocket_ = std::make_unique<cocos2d::network::WebSocket>();
    socketDelegate_ = std::make_unique<SocketDelegate>();
    socketDelegate_->registerObserver(this);

    this->eventObserverI_ = nullptr;

    initConnection();

    //TODO checking if connsection suceded etc.

}

void NetworkManager::receiveMessage(std::shared_ptr<rapidjson::Document> jsonMessage) {

    if(eventObserverI_ != nullptr){
        eventObserverI_->receiveMessage(jsonMessage);
    }else{
        std::cout << "[NetworkManager] Implement buffor for messages, message lost." << std::endl;
    }
}

void NetworkManager::registerObserver(EventObserverI *eventObserverI) {
    this->eventObserverI_ = eventObserverI;
}

void NetworkManager::initConnection() {
    webSocket_->init(*socketDelegate_, url_ , &protocols_);
}

void NetworkManager::unregisterObserver() {
    this->eventObserverI_ = nullptr;
}

void NetworkManager::sendMessage(std::shared_ptr<rapidjson::Document> jsonMessage) {

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    jsonMessage->Accept(writer);
    std::string toSend  = buffer.GetString();
   // std::cout << "[NetowrkManafer] sending message: " << toSend << std::endl;

    webSocket_->send(toSend);
}

void NetworkManager::sendMessage(std::shared_ptr<std::string> strMessage) {
    webSocket_->send(*strMessage);
}
