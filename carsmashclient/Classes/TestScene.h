//
// Created by peter on 4/6/18.
//

#ifndef MYGAME_TESTSCENE_H
#define MYGAME_TESTSCENE_H


#include "cocos2d.h"
#include "network/SocketIO.h"
#include "network/WebSocket.h"




class TestScene : public cocos2d::Scene{
public:

    static cocos2d::Scene* createScene();
    virtual bool init();

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(TestScene)
private:

    cocos2d::network::WebSocket * websocket;
    std::string toSend_;

};


#endif //MYGAME_TESTSCENE_H
