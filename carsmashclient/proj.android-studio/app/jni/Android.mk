LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES :=$(LOCAL_PATH)/hellocpp/main.cpp \
                  $(LOCAL_PATH)/../../../Classes/AppDelegate.cpp \
                  $(LOCAL_PATH)/../../../Classes/HelloWorldScene.cpp \
                  $(LOCAL_PATH)/../../../Classes/scenes/gameScene/GameScene.cpp \
                  $(LOCAL_PATH)/../../../Classes/utils/toStringFix.h \
                  Classes/network/SocketDelegate.h $(LOCAL_PATH)/../../../Classes/scenes/MainMenuScene.cpp \
                  $(LOCAL_PATH)/../../../Classes/AppDelegate.h $(LOCAL_PATH)/../../../Classes/HelloWorldScene.h $(LOCAL_PATH)/../../../Classes/TestScene.h $(LOCAL_PATH)/../../../Classes/network/SocketDelegate.cpp $(LOCAL_PATH)/../../../Classes/network/SocketDelegate.h $(LOCAL_PATH)/../../../Classes/scenes/MainMenuScene.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/GameScene.h $(LOCAL_PATH)/../../../Classes/Test2.h $(LOCAL_PATH)/../../../Classes/utils/toStringFix.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/HUD/Arrows.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/HUD/Arrows.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/HUD/ArrowsObserverI.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/HUD/ArrowsObserverI.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/controller  $(LOCAL_PATH)/../../../Classes/scenes/gameScene/controller/GameController.h  $(LOCAL_PATH)/../../../Classes/scenes/gameScene/controller/GameController.cpp $(LOCAL_PATH)/../../../Classes/network/NetworkManager.cpp $(LOCAL_PATH)/../../../Classes/network/NetworkManager.h $(LOCAL_PATH)/../../../Classes/network/EventObserverI.cpp $(LOCAL_PATH)/../../../Classes/network/EventObserverI.h $(LOCAL_PATH)/../../../Classes/utils/makeUnique.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/GameRenderer.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/GameRenderer.h $(LOCAL_PATH)/../../../Classes/utils/ServiceProvider.cpp $(LOCAL_PATH)/../../../Classes/utils/ServiceProvider.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/EntityRenderer.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/EntityRenderer.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/ColorsPicker.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/ColorsPicker.h $(LOCAL_PATH)/../../../Classes/utils/scaleManager/ScaleManager.cpp $(LOCAL_PATH)/../../../Classes/utils/scaleManager/ScaleManager.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/DrawNodePool.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/DrawNodePool.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/MiniMapRenderer.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/MiniMapRenderer.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/TopTableRenderer.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/TopTableRenderer.h $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/LabelPool.cpp $(LOCAL_PATH)/../../../Classes/scenes/gameScene/renderer/LabelPool.h

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
