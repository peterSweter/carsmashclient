#include "../Classes/AppDelegate.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <utils/ServiceProvider.h>

USING_NS_CC;

int main(int argc, char **argv)
{
    // create the application instance

    if(argc > 1){
        ServiceProvider::serverIP_ = argv[1];
    }

    AppDelegate app;
    return Application::getInstance()->run();
}
